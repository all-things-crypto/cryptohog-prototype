import BinanceAPI from 'node-binance-api';

type TradeSymbol = string;
type TradeSymbolList = TradeSymbol[];

class TradableSymbolPair {
  public readonly base: TradeSymbol;
  public readonly quote: TradeSymbol;
  constructor(base: TradeSymbol, quote: TradeSymbol) {
    this.base = base;
    this.quote = quote;
  }

  public toString(): string {
    return `${this.quote}${this.base}`;
  }
}

interface TradableSymbolsPairMap {
  [baseQuoteSymbol: string]: TradableSymbolPair;
}

interface TradableSymbolsPairBaseToQuoteMap {
  [baseSymbol: string]: { [quoteSymbol: string]: TradableSymbolPair };
}

// tslint:disable-next-line:max-classes-per-file
class TradableArbitrageTrianglePath {
  public readonly startNode: TradableSymbolPair;
  public readonly intermediateNode: TradableSymbolPair;
  public readonly endNode: TradableSymbolPair;
  constructor(
    startNode: TradableSymbolPair,
    intermediateNode: TradableSymbolPair,
    endNode: TradableSymbolPair,
  ) {
    this.startNode = startNode;
    this.intermediateNode = intermediateNode;
    this.endNode = endNode;
  }
}

interface TradableArbitrageTrianglePathMap {
  [path: string]: TradableArbitrageTrianglePath;
}

interface TradeSymbolMarketDepth {
  bids: { [value: number]: number }; // TODO: Convert to BigNumber.js
  asks: { [value: number]: number }; // TODO: Convert to BigNumber.js
  bestBid: number; // TODO: Convert to BigNumber.js
  bestAsk: number; // TODO: Convert to BigNumber.js
  timestamp: Date;
}

interface TradeSymbolMarketDepthMap {
  [symbol: string]: TradeSymbolMarketDepth;
}

const queryTradablePairs = ({
  api,
  baseSymbols,
}: {
  api: BinanceAPI;
  baseSymbols: TradeSymbolList;
}): Promise<TradableSymbolsPairBaseToQuoteMap> => {
  return new Promise((resolve, reject) => {
    api.prevDay(false, (error, ticker) => {
      if (error) {
        return reject(error);
      }

      const result = ticker.reduce<TradableSymbolsPairBaseToQuoteMap>((prev, current) => {
        const symbol = current.symbol;

        const findBaseSymbol = (): string | null => {
          const found = baseSymbols.filter(item => symbol.endsWith(item));
          if (!found || !found.length) {
            return null;
          }
          return found[0];
        };

        const baseSymbol = findBaseSymbol();
        if (!baseSymbol) {
          return prev;
        }

        const symbolBaseIdx = symbol.lastIndexOf(baseSymbol);
        const base = symbol.substring(symbolBaseIdx);
        const quote = symbol.substring(0, symbolBaseIdx);

        if (!Reflect.has(prev, base)) {
          prev[base] = {};
        }

        prev[base] = {
          ...prev[base],
          [quote]: new TradableSymbolPair(base, quote),
        };

        return prev;
      }, {});

      resolve(result);
    });
  });
};

const findTradableSymbolPathes = ({
  startEndTradingSymbol,
  tradableSymbolPairs,
}: {
  startEndTradingSymbol: TradeSymbol;
  tradableSymbolPairs: TradableSymbolsPairBaseToQuoteMap;
}): TradableArbitrageTrianglePathMap => {
  if (!Reflect.has(tradableSymbolPairs, startEndTradingSymbol)) {
    return {};
  }

  const excludeTradablePairs = (
    excludeBaseTradingSymbol: TradeSymbol,
    tradablePairs: TradableSymbolsPairBaseToQuoteMap,
  ): TradableSymbolsPairBaseToQuoteMap => {
    return Object.keys(tradablePairs).reduce<TradableSymbolsPairBaseToQuoteMap>((prev, current) => {
      if (current === excludeBaseTradingSymbol) {
        return prev;
      }
      return { ...prev, [current]: tradablePairs[current] };
    }, {});
  };

  const startEndTradablePairsMap = tradableSymbolPairs[startEndTradingSymbol];
  const intermediateTradablePairs = excludeTradablePairs(
    startEndTradingSymbol,
    tradableSymbolPairs,
  );

  const result: TradableArbitrageTrianglePathMap = {};
  for (const startQuoteSymbol of Object.keys(startEndTradablePairsMap)) {
    const startNode = startEndTradablePairsMap[startQuoteSymbol];

    // NOTE: Try to find the start quote symbol within the intermediate tradable symbol maps
    for (const intermediateBaseSymbol of Object.keys(intermediateTradablePairs)) {
      const intermediateTradablePairsMap = intermediateTradablePairs[intermediateBaseSymbol];
      if (!Reflect.has(intermediateTradablePairsMap, startQuoteSymbol)) {
        continue;
      }

      const intermediateNode = intermediateTradablePairsMap[startQuoteSymbol];

      // NOTE: Check if the intermediate node is also a tradable option for our start node
      if (!Reflect.has(startEndTradablePairsMap, intermediateNode.base)) {
        continue;
      }

      // NOTE: We found a winner!
      const endNode = startEndTradablePairsMap[intermediateNode.base];

      result[
        `${startNode.base}-${intermediateNode.quote}-${intermediateNode.base}-${endNode.base}`
      ] = new TradableArbitrageTrianglePath(startNode, intermediateNode, endNode);
    }
  }

  return result;
};

const collectStreamNames = (pathes: TradableArbitrageTrianglePathMap): TradableSymbolsPairMap => {
  const result: TradableSymbolsPairMap = {};

  for (const pathName of Object.keys(pathes)) {
    const path = pathes[pathName];
    result[path.startNode.toString()] = path.startNode;
    result[path.intermediateNode.toString()] = path.intermediateNode;
    result[path.endNode.toString()] = path.endNode;
  }

  return result;
};

const createMarketDepth = ({
  depth,
  api,
}: {
  depth: {
    asks: { [price: number]: number };
    bids: { [price: number]: number };
    eventTime: number;
  };
  api: BinanceAPI;
}): TradeSymbolMarketDepth => {
  const bids = api.sortBids(depth.bids);
  const asks = api.sortAsks(depth.asks);
  return {
    bids,
    asks,
    bestBid: parseFloat(api.first(bids)),
    bestAsk: parseFloat(api.first(asks)),
    timestamp: depth.eventTime ? new Date(depth.eventTime) : new Date(),
  };
};

type TradeSymbolMarketDepthUpdateListenerFn = ({
  service,
  symbol,
  marketDepth,
}: {
  service: TradeSymbolMarketDepthService;
  symbol: TradeSymbol;
  marketDepth: TradeSymbolMarketDepth;
}) => void;

interface TradeSymbolMarketDepthUpdateListenerMap<
  T extends TradeSymbolMarketDepthUpdateListenerFn | TradeSymbolMarketDepthUpdateListenerFn[]
> {
  [symbol: string]: T;
}

// tslint:disable-next-line:max-classes-per-file
class TradeSymbolMarketDepthService {
  private marketDepthMap: TradeSymbolMarketDepthMap = {};
  private listener: TradeSymbolMarketDepthUpdateListenerMap<
    TradeSymbolMarketDepthUpdateListenerFn[]
  > = {};

  public getMarketDepth(symbol: TradeSymbol): TradeSymbolMarketDepth | null {
    if (!Reflect.has(this.marketDepthMap, symbol)) {
      return null;
    }

    return this.marketDepthMap[symbol];
  }

  constructor() {}

  public update({
    symbol,
    marketDepth,
  }: {
    symbol: string;
    marketDepth: TradeSymbolMarketDepth;
  }): void {
    this.marketDepthMap[symbol] = marketDepth;
    this.emit(symbol, marketDepth);
  }

  public registerUpdateListener({
    symbol,
    callbackFn,
  }: {
    symbol: TradeSymbol;
    callbackFn: TradeSymbolMarketDepthUpdateListenerFn;
  }): void {
    if (!Reflect.has(this.listener, symbol)) {
      this.listener[symbol] = [];
    }

    const listener = this.listener[symbol];
    // NOTE: We explicitly allow multiple "same" listeners to one symbol, as its used by the TradePath
    listener.push(callbackFn);
  }

  private emit(symbol: TradeSymbol, marketDepth: TradeSymbolMarketDepth): void {
    if (!Reflect.has(this.listener, symbol)) {
      return;
    }

    const listeners = this.listener[symbol];
    for (const listener of listeners) {
      listener({ service: this, symbol, marketDepth });
    }
  }
}

type TradingPathMarketDepthFn = ({
  accountService,
  tradingPath,
  marketDepthMap,
}: {
  accountService: AccountService;
  tradingPath: TradableArbitrageTrianglePath;
  marketDepthMap: TradeSymbolMarketDepthMap;
}) => void;

const createTradingPathMarketDepthListeners = ({
  accountService,
  tradingPath,
  callbackFn,
}: {
  accountService: AccountService;
  tradingPath: TradableArbitrageTrianglePath;
  callbackFn: TradingPathMarketDepthFn;
}): TradeSymbolMarketDepthUpdateListenerMap<TradeSymbolMarketDepthUpdateListenerFn> => {
  const startNodeSymbol = tradingPath.startNode.toString();
  const intermediateNodeSymbol = tradingPath.intermediateNode.toString();
  const endNodeSymbol = tradingPath.endNode.toString();

  let marketDepthMap: TradeSymbolMarketDepthMap = {};

  const updateListener = ({
    symbol,
    marketDepth,
  }: {
    service: TradeSymbolMarketDepthService;
    symbol: TradeSymbol;
    marketDepth: TradeSymbolMarketDepth;
  }): void => {
    marketDepthMap[symbol] = marketDepth;

    // NOTE: Check if we actually got all path related depth maps updated, if so emit the callback
    if (
      Reflect.has(marketDepthMap, startNodeSymbol) &&
      Reflect.has(marketDepthMap, intermediateNodeSymbol) &&
      Reflect.has(marketDepthMap, endNodeSymbol)
    ) {
      callbackFn({ accountService, tradingPath, marketDepthMap });
      // NOTE: Clean up for a fresh start, as we only want the callback to call if every node symbol has been updated!
      marketDepthMap = {};
    }
  };

  return {
    [startNodeSymbol]: updateListener,
    [intermediateNodeSymbol]: updateListener,
    [endNodeSymbol]: updateListener,
  };
};

interface AssetsMap {
  [symbol: string]: number;
}

// tslint:disable-next-line:max-classes-per-file
class AccountService {
  private assets: AssetsMap = {};

  constructor(initialAssetsMap: AssetsMap) {
    this.assets = { ...initialAssetsMap };
  }

  public getValue(symbol: TradeSymbol): number {
    if (!Reflect.has(this.assets, symbol)) {
      return 0;
    }

    return this.assets[symbol];
  }

  public deposit(symbol: TradeSymbol, value: number): void {
    if (value <= 0) {
      return;
    }

    if (!Reflect.has(this.assets, symbol)) {
      this.assets[symbol] = 0;
    }

    this.assets[symbol] += value;
  }

  public cashOut(symbol: TradeSymbol, value?: number): number {
    if (!Reflect.has(this.assets, symbol)) {
      return 0;
    }

    const currentValue = this.assets[symbol];
    const newValue = currentValue - (value || currentValue);
    if (newValue < 0) {
      return 0;
    }

    this.assets[symbol] = newValue;
    return currentValue - newValue;
  }
}

const tradingPathCostsCalculator = ({
  accountService,
  tradingPath,
  marketDepthMap,
}: {
  accountService: AccountService;
  tradingPath: TradableArbitrageTrianglePath;
  marketDepthMap: TradeSymbolMarketDepthMap;
}): void => {
  const startNodeMarketDepth = marketDepthMap[tradingPath.startNode.toString()];
  const intermediateNodeMarketDepth = marketDepthMap[tradingPath.intermediateNode.toString()];
  const endNodeMarketDepth = marketDepthMap[tradingPath.endNode.toString()];

  // console.log(
  //   `TradePath cost calculation for:  ${tradingPath.startNode.toString()} -> ${tradingPath.intermediateNode.toString()} -> ${tradingPath.endNode.toString()}`,
  // );

  // console.log(
  //   `${tradingPath.startNode.toString()} - BestAsk: ${startNodeMarketDepth.bestAsk} | BestBid: ${
  //     startNodeMarketDepth.bestBid
  //   }`,
  // );
  // console.log(
  //   `${tradingPath.intermediateNode.toString()} - BestAsk: ${
  //     intermediateNodeMarketDepth.bestAsk
  //   } | BestBid: ${intermediateNodeMarketDepth.bestBid}`,
  // );
  // console.log(
  //   `${tradingPath.endNode.toString()} - BestAsk: ${endNodeMarketDepth.bestAsk} | BestBid: ${
  //     endNodeMarketDepth.bestBid
  //   }`,
  // );

  // STEP 1: Check if our account has some balance > 0 of the start node symbol
  const startNodeSymbolBalance = accountService.getValue(tradingPath.startNode.base);
  if (startNodeSymbolBalance <= 0) {
    // NOTE: Nothing here we could possible trade with
    console.error(
      `Account is missing a positive balance of the start node symbol: ${tradingPath.startNode.toString()} = ${startNodeSymbolBalance.toFixed(
        8,
      )}`,
    );
    return;
  }

  /**
   * IMPORTANT: As checking against the online Binance trading interface, some values you can trade with are **truncated**
   * => thus one is not able to trade with every asset in its *full* range of decimal places (8). ETH/BTC only allows 3 for ETH and BTC only 6 decimal places
   * => thus the possible GAINS could be potential smaller if any.. so we have to check against every trade symbol what are maximal decimal places are!!!11elf
   *
   * TODO: Introduce a lookup table for every symbol with their respective decimal places and pass it into here!
   */
  const truncate = (value: number, precision: number): number => {
    const step = Math.pow(10, precision || 0);
    const temp = Math.trunc(step * value);
    return temp / step;
  };

  // STEP 2: Let's **buy** some of the start node quote symbol asset
  // TODO: Add **Taker** Fees!
  const startNodeQuoteBalance = startNodeSymbolBalance / startNodeMarketDepth.bestAsk;
  // console.log(`${tradingPath.startNode.quote}: ${startNodeQuoteBalance.toFixed(8)}`);

  // STEP 3: Let's **sell** all of the previous bought start asset and thus get the intermediate node quote symbol asset
  // TODO: Add **Maker** Fees!
  const startNodeQuoteBalanceTruncated = startNodeQuoteBalance; // truncate(startNodeQuoteBalance, 3);
  const intermediateNodeBaseBalance =
    intermediateNodeMarketDepth.bestBid * startNodeQuoteBalanceTruncated;
  // console.log(`${tradingPath.intermediateNode.base}: ${intermediateNodeBaseBalance.toFixed(8)}`);

  // STEP 4: Now let's **sell** our intermediate asset and get back our start base symbol asset
  // TODO: Add **Maker** Fees!
  const intermediateNodeBaseBalanceTruncated = intermediateNodeBaseBalance; // truncate(intermediateNodeBaseBalance, 6);
  const endNodeAssetBaseBalance = endNodeMarketDepth.bestBid * intermediateNodeBaseBalanceTruncated;
  // console.log(`${tradingPath.endNode.base}: ${endNodeAssetBaseBalance.toFixed(8)}`);

  // STEP 5: Check if we actually would make some gains or a loss
  const result = endNodeAssetBaseBalance - startNodeSymbolBalance;
  const percentage = (result / startNodeSymbolBalance) * 100;
  // if (result > 0) {
  //   console.log(`GAINS: ${result.toFixed(8)} (${percentage.toFixed(4)}%)`);
  //   if (percentage >= 0.04) {
  //     console.log(`TRADE OPPORTUNITY!`);
  //   }
  // } else {
  //   console.log(
  //     `LOSS: ${result.toFixed(8)} (${percentage.toFixed(4)}%) ${tradingPath.endNode.base}`,
  //   );
  // }

  if (result > 0 && percentage >= 0.04) {
    console.log(`===== TRADE OPPORTUNITY =====`);
    console.log(
      `Cost calculation for:  ${tradingPath.startNode.toString()} -> ${tradingPath.intermediateNode.toString()} -> ${tradingPath.endNode.toString()}`,
    );

    console.log(
      `${tradingPath.startNode.toString()} - BestAsk: ${startNodeMarketDepth.bestAsk} | BestBid: ${
        startNodeMarketDepth.bestBid
      }`,
    );
    console.log(
      `${tradingPath.intermediateNode.toString()} - BestAsk: ${
        intermediateNodeMarketDepth.bestAsk
      } | BestBid: ${intermediateNodeMarketDepth.bestBid}`,
    );
    console.log(
      `${tradingPath.endNode.toString()} - BestAsk: ${endNodeMarketDepth.bestAsk} | BestBid: ${
        endNodeMarketDepth.bestBid
      }`,
    );
    console.log(`${tradingPath.startNode.quote}: ${startNodeQuoteBalance.toFixed(8)}`);
    console.log(`${tradingPath.intermediateNode.base}: ${intermediateNodeBaseBalance.toFixed(8)}`);
    console.log(`${tradingPath.endNode.base}: ${endNodeAssetBaseBalance.toFixed(8)}`);
    console.log(`-----------------------------`);
    console.log(
      `GAINS: ${percentage.toFixed(4)}% => ${result.toFixed(8)} ${tradingPath.endNode.base}`,
    );
    console.log(`=============================`);
  }
};

const main = async () => {
  const startTradingSymbol = 'USDT';
  const intermediateTradingSymbols = ['ETH', 'BTC'];

  // NOTE: We use a fictional assets value as our main asset, so we are able to "trade"
  const accountService = new AccountService({ [startTradingSymbol]: 1000.0 });
  const marketDepthService = new TradeSymbolMarketDepthService();

  const api = new BinanceAPI();
  const tradableSymbolPairs = await queryTradablePairs({
    api,
    baseSymbols: [...intermediateTradingSymbols, startTradingSymbol],
  });
  console.log(tradableSymbolPairs);

  const tradablePathes = findTradableSymbolPathes({
    startEndTradingSymbol: startTradingSymbol,
    tradableSymbolPairs,
  });

  console.log(`Found ${Object.keys(tradablePathes).length} tradable pathes`);
  console.log(`===========================================================`);

  const streamNamesSet = new Set<TradeSymbol>();
  for (const pathName of Object.keys(tradablePathes)) {
    const tradingPath = tradablePathes[pathName];
    console.log(
      `${pathName}: ${tradingPath.startNode.toString()} -> ${tradingPath.intermediateNode.toString()} -> ${tradingPath.endNode.toString()}`,
    );

    const depthMarketUpdateListeners = createTradingPathMarketDepthListeners({
      accountService,
      tradingPath,
      callbackFn: tradingPathCostsCalculator,
    });
    for (const symbol of Object.keys(depthMarketUpdateListeners)) {
      streamNamesSet.add(symbol);
      const listener = depthMarketUpdateListeners[symbol];
      marketDepthService.registerUpdateListener({ symbol, callbackFn: listener });
    }
  }
  const streamNames = [...streamNamesSet];

  // TODO: Register all possible pathes with their respective handlers
  const handle = api.websockets.depthCache(
    streamNames,
    (symbol: string, depth: any) => {
      const marketDepth = createMarketDepth({ depth, api });
      marketDepthService.update({ symbol, marketDepth });
    },
    10,
  );
};

main();
